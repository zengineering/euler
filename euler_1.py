#!/usr/bin/env python3

# 
# Project Euler: Problem 1
#
# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
# Find the sum of all the multiples of 3 or 5 below 1000.
#

def sum_of_multiples(limit, *bases):
  msum = 0
  for b in bases:
    i=0
    mult=0
    while mult < limit:
      msum += mult
      i += 1
      mult = i*b
  return msum

 
def test():
  tests = [
    {'limit' : 10, 'bases' : [3,5]},
    {'limit' : 20, 'bases' : [4,7]},
    {'limit' : 1000, 'bases' : [3,5]},
  ]

  for t in tests:
    print("Sum of the multiples of {} less than {}: {}".format(t['bases'], t['limit'], sum_of_multiples(t['limit'], *t['bases'])))


if __name__ == '__main__':
  test()
